import config from "./config.json";

export async function getProducts() {
  const result = await fetch(`${config.url}/products`);
  const resultJson = await result.json();
  return resultJson;
}
