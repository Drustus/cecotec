import React from "react";

import "./style.scss";

const Spinner = () => (
  <div className="columns is-centered padding">
    <span className="icon is-large">
      <div className="column">
        <i className="fas fa-spinner fa-3x fa-pulse"></i>
      </div>
    </span>
  </div>
);

export default Spinner;
