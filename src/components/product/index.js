import React from "react";
import "./styles.scss";

import ProductButton from "../productButton";

const Product = ({ title, subtitle, image, onEdit, onDelete }) => {
  return (
    <div className="column is-one-quarter">
      <div className="card">
        <div className="card-header">
          <div className="card-image">
            <figure className="image">
              <img src={image} />
            </figure>
          </div>
        </div>
        <div className="card-content">
          <p className="title is-6 has-text-centered">{title}</p>
          <p className="subtitle is-6">{subtitle}</p>
        </div>
        {onDelete && onEdit ? (
          <footer className="card-footer">
            <ProductButton onClick={onEdit}>Editar</ProductButton>
            <ProductButton onClick={onDelete}>Borrar</ProductButton>
          </footer>
        ) : null}
      </div>
    </div>
  );
};

export default Product;
