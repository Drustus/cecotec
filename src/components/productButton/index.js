import React from "react";

export const ProductButton = ({ onClick, children }) => {
  const click = e => {
    e.preventDefault();
    onClick();
  };
  return (
    <a href="#" className={"card-footer-item"} onClick={click}>
      {children}
    </a>
  );
};

export default ProductButton;
