import React from "react";

const Screen = ({ title, children }) => {
  return (
    <div className="section">
      {title && <h1 className="title has-text-centered">{title}</h1>}
      {children}
    </div>
  );
};

export default Screen;
