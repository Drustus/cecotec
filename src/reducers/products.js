// Based on duck architecture pattern
import { takeLatest, call, put } from "redux-saga/effects";
import { createSelector } from "reselect";
import makeActionCreator from "./utils/makeActionCreator";
import { getProducts as getProductsApi } from "../api/get";

// Actions
const LOAD = "cecotec/products/LOAD";
const SET = "cecotec/products/SET";
const DELETE = "cecotec/products/DELETE";
const EDIT = "cecotec/products/EDIT";

// Actions creators
export const loadProducts = makeActionCreator(LOAD);
export const setProducts = makeActionCreator(SET, "products");
export const deleteProduct = makeActionCreator(DELETE, "id");
export const editProduct = makeActionCreator(EDIT, "product");

// Reducer
const deleteProd = (state, id) => {
  const products = state.loaded.slice();
  const productIndex = products.findIndex(el => el.id === id);
  products.splice(productIndex, 1);
  return {
    ...state,
    loaded: products
  };
};

function changeProduct(state, product) {
  const products = state.loaded.slice();
  let index = products.findIndex(p => p.id === product.id);
  products[index] = product;
  return { ...state, loaded: products };
}

export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case SET:
      return { ...state, loaded: action.products };
    case DELETE:
      return deleteProd(state, action.id);
    case EDIT:
      return changeProduct(state, action.product);
    default:
      return state;
  }
}

// Sagas

function* getProducts() {
  try {
    const products = yield call(getProductsApi);
    yield put(setProducts(products));
  } catch (error) {
    // yield put(push("error"));
  }
}

export function* productsSaga() {
  yield takeLatest(LOAD, getProducts);
}

// Selectors

const baseSelector = state => state.products;

export const productsSelector = createSelector(
  baseSelector,
  products => products.loaded
);

export const productSelector = createSelector(
  baseSelector,
  (_, productId) => productId,
  (products, productId) =>
    products.loaded && products.loaded.find(product => product.id === productId)
);
