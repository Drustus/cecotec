import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";

import { Screen, Spinner, Product } from "../../components";

import {
  loadProducts,
  deleteProduct,
  productsSelector
} from "../../reducers/products";

const Home = () => {
  const dispatch = useDispatch();
  const products = useSelector(productsSelector);

  useEffect(() => {
    if (!products) {
      dispatch(loadProducts());
    }
  }, []);

  const onDelete = id => {
    dispatch(deleteProduct(id));
  };

  const onEdit = id => {
    dispatch(push({ pathname: `/edit/${id}` }));
  };

  return (
    <Screen title="Tus productos">
      {!products ? (
        <Spinner />
      ) : (
        <div className="columns is-multiline">
          {products.map(product => (
            <Product
              key={product.id}
              {...product}
              onEdit={() => onEdit(product.id)}
              onDelete={() => onDelete(product.id)}
            />
          ))}
        </div>
      )}
    </Screen>
  );
};

export default Home;
