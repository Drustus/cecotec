import React from "react";
import { act } from "react-dom/test-utils";
import { mount } from "enzyme";
import Edit from "../edit";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn(),
  useSelector: () => ({ id: "1", title: "title 1", subtitle: "subtitle 1" })
}));
jest.mock("connected-react-router", () => ({
  push: jest.fn()
}));

let props = {};

const getWrapper = () => mount(<Edit {...props} />);

describe("Edit component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should render", () => {
    const wrapper = getWrapper();
    expect(wrapper).toMatchSnapshot();
  });

  it("should change value", () => {
    const wrapper = getWrapper();

    act(() => {
      input(wrapper).onChange("testValue");
    });

    wrapper.update();

    expect(input(wrapper).value).toEqual("testValue");
  });
});

const input = wrapper =>
  wrapper
    .find("Input")
    .at(0)
    .props();
