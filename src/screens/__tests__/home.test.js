import React from "react";
import { shallow } from "enzyme";
import Home from "../home";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn(),
  useSelector: () => [
    { id: "1", title: "title 1", subtitle: "subtitle 1" },
    { id: "2", title: "title 2", subtitle: "subtitle 2" }
  ]
}));
jest.mock("connected-react-router", () => ({
  push: jest.fn()
}));

let props = {};

const getWrapper = () => shallow(<Home {...props} />);

describe("Home component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should render", () => {
    const wrapper = getWrapper();
    expect(wrapper).toMatchSnapshot();
  });
});
