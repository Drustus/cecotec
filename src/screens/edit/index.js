import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { push } from "connected-react-router";

import { productSelector, editProduct } from "../../reducers/products";
import { Product, Input } from "../../components";

const Edit = ({ match }) => {
  const dispatch = useDispatch();
  const initialProduct = useSelector(state =>
    productSelector(state, match.params.productId)
  );
  const [product, setProduct] = useState(initialProduct);

  const onChange = (field, value) => {
    const modifiedProduct = { ...product };
    modifiedProduct[field] = value;
    setProduct(modifiedProduct);
  };

  if (!initialProduct) {
    dispatch(push("/"));
  }

  const onSave = e => {
    e.preventDefault();
    dispatch(editProduct(product));
    dispatch(push("/"));
  };

  const onCancel = e => {
    e.preventDefault();
    dispatch(push("/"));
  };

  return !initialProduct ? null : (
    <div className="section">
      <div className="columns is-centered is-multiline">
        <Product key={product.id} {...product} />
      </div>
      <form>
        <label className="label">Título</label>
        <div className="control">
          <Input value={product.title} onChange={v => onChange("title", v)} />
        </div>
        <label className="label">Subtítulo</label>
        <div className="control">
          <Input
            value={product.subtitle}
            onChange={v => onChange("subtitle", v)}
          />
        </div>
        <label className="label">Imagen Url</label>
        <div className="control">
          <Input value={product.image} onChange={v => onChange("image", v)} />
        </div>
        <div className="field is-grouped is-grouped-right">
          <div className="control">
            <button className="button is-link" type="submit" onClick={onSave}>
              Guardar
            </button>
          </div>
          <div className="control">
            <button className="button is-text" onClick={onCancel}>
              Cancelar
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Edit;
