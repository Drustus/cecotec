import React from "react";
import { Route } from "react-router-dom";

import Home from "./screens/home";
import Edit from "./screens/edit";

const App = () => {
  return (
    <div>
      <main>
        <Route exact path="/" component={Home} />
        <Route exact path="/edit/:productId" component={Edit} />
      </main>
    </div>
  );
};

export default App;
