import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import store from "./store";
import history from "./history";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

if (document.getElementById("root"))
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>,
    document.getElementById("root")
  );
serviceWorker.unregister();
