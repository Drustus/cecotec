# Prueba técnica Cecotec front-end

## Scripts

### `yarn start-server`

**Lanzarlo en primer lugar.** Ejecuta el servicio dummy que hace de API para obtener los proyectos. Tiene por defecto un **delay de 1.5 seg.**

### `yarn start`

Ejecuta la aplicación.

### `yarn test`

Ejecuta los **tests unitarios** en modo watch.

### `yarn test:coverage`

Ejecuta los **tests unitarios** una única vez y obtiene la cobertura de código.

Se puede ver la cobertura en consola o abriendo en un navegador el index.html que hay en la carpeta _coverage/lcov-report_

## Anotaciones

- Para los **estilos** he obtado por [Bulma](https://bulma.io/) para simplificar.

- **No** he hecho **hincapié en los errores**. Por ejemplo, he supuesto que el back no va a fallar y no hay validaciones en el formulario.

- Sobre los tests, he creado un par de tests unitarios muy simples que usan Jest y Enzyme. Además también se podría optar por Cypress para los tests funcionales, con el cual estoy algo familiarizado.

- No existe persistencia de los cambios.

- No he especifidado las propiedades (propTypes) de los componentes por rapidez. Se podría hacer con las base de React o añadiendo Flow.
